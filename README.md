# frcbstats

This package implements tools for the testing of U-shaped regression functions
using critical bandwidth, developed in the paper
[Non-Parametric Testing of U-Shaped Relationships](https://ssrn.com/abstract=2905833):

    Kostyshak, Scott, Non-Parametric Testing of U-Shaped Relationships
    Available at SSRN: https://ssrn.com/abstract=2905833

# example


```
library(frcbstats)

x <- rnorm(100)
y <- x^2 + rnorm(100)
dataf <- data.frame(x = x, y = y)

ex1_q <- cbquasi(y ~ U(x), data = dataf)
ex1_m <- cbmono(y ~ U(x), data = dataf)

ex1_q$p
ex1_m$p
```

A large p-value for the null hypothesis of quasi-convexity and a small p-value for
the null of monotonicity is consistent with a regression function that is
quasi-convex but not monotone.
