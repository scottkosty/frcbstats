context("utilities")

test_that("lmax behaves well", {
  vec1 <- c(0,1,0,1,0,1,0,1)
  expect_identical(lmax(vec1)[["nbumps"]], 3L)
  expect_identical(lmax(vec1, l = 2L)[["nbumps"]], 0L)
  vec1.5 <- c(0,0,1,0,0,1,0,0,1,0,0,1)
  expect_identical(lmax(vec1.5)[["nbumps"]], 3L)
  expect_identical(lmax(vec1.5, l = 2L)[["nbumps"]], 3L)
  expect_identical(lmax(vec1.5, l = 3L)[["nbumps"]], 0L)
})

test_that("there are zero bumps for a flat vector", {
  vec2 <- c(1,1,1,1,1,1,1,1)
  expect_identical(lmax(vec2)[["nbumps"]], 0L)
  expect_identical(lmax(vec2, l = 2L)[["nbumps"]], 0L)
})

test_that("there are zero bumps for a monotone vector", {
  vec3 <- 1:10
  expect_identical(lmax(vec3)[["nbumps"]], 0L)
  expect_identical(lmax(vec3, l = 2L)[["nbumps"]], 0L)
})

test_that("there is 0 bumps and 1 valley in a U", {
  vec4 <- c(5, 4, 3, 2, 1, 0, 1, 2, 3, 4, 5)
  expect_identical(lmax(vec4)[["nbumps"]], 0L)
  expect_identical(lmax(vec4, l = 4L)[["nbumps"]], 0L)
  expect_identical(lmax(-vec4)[["nbumps"]], 1L)
  expect_identical(lmax(-vec4, l = 4L)[["nbumps"]], 1L)
})

test_that("lmax behaves well for small vectors", {
  wgrep <- "No comparison"
  vec5 <- c()
  expect_warning(ret_ <- lmax(vec5)[["nbumps"]], wgrep)
  expect_identical(ret_, 0L)
  expect_warning(ret_ <- lmax(vec5, l = 3)[["nbumps"]], wgrep)
  expect_identical(ret_, 0L)
  vec6 <- c(0)
  expect_warning(ret_ <- lmax(vec6)[["nbumps"]], wgrep)
  expect_warning(ret_ <- lmax(vec6, l = 3)[["nbumps"]], wgrep)
  expect_identical(ret_, 0L)
  vec7 <- c(0, 1)
  expect_warning(ret_ <- lmax(vec7)[["nbumps"]], wgrep)
  expect_warning(ret_ <- lmax(vec7, l = 3)[["nbumps"]], wgrep)
  expect_identical(ret_, 0L)
  vec8 <- c(0, 1, 0)
  expect_identical(lmax(vec8)[["nbumps"]], 1L)
  expect_warning(ret_ <- lmax(vec8, l = 2)[["nbumps"]], wgrep)
  expect_identical(ret_, 0L)
  vec9 <- c(0, 1, 2, 0)
  expect_identical(lmax(vec9)[["nbumps"]], 1L)
  expect_warning(ret_ <- lmax(vec9, l = 2)[["nbumps"]], wgrep)
  expect_identical(ret_, 0L)
  # TODO give a warning if no check for bumps (e.g. l so large not even one
  # checked).
})

test_that("K argument works well", {
  vec10 <- c(0, 1, 0, 1, 0, 1, 0, 1)
  expect_identical(lmax(vec10)[["nbumps"]], 3L)
  expect_identical(lmax(vec10, K = 1)[["nbumps"]], 1L)
  expect_identical(lmax(vec10, K = 2)[["nbumps"]], 2L)
})

test_that("Ties are dealt with", {
  # TODO think about ties when l != 1
  vec11 <- c(0, 0, 1, 1, 0, 0, 0, 0)
  expect_identical(lmax(vec11)[["nbumps"]], 1L)
  vec12 <- c(0, 0, 1, 1, 1, 0, 0, 0)
  expect_identical(lmax(vec12)[["nbumps"]], 1L)
  vec13 <- c(0, 0, 1, 1, 1, 1, 0, 0)
  expect_identical(lmax(vec13)[["nbumps"]], 1L)
  vec14 <- c(0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0)
  expect_identical(lmax(vec14)[["nbumps"]], 1L)
  # vec15 shows necessity to reset the flag
  vec15 <- c(0, 1, 2, 2, 3, 4, 3, 2, 2, 1, 0, 0, 0)
  expect_identical(lmax(vec15)[["nbumps"]], 1L)
})

# TODO test buffer arg
